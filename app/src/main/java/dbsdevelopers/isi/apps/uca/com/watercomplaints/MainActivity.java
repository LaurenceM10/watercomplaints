package dbsdevelopers.isi.apps.uca.com.watercomplaints;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tumblr.remember.Remember;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.ComplaintAddActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.LoginActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.ProfileActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments.CategoriesFragment;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments.ComplaintsFragment;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments.MyComplaintsFragment;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private NavigationView navigationView;

    //To menu info
    private ImageView avatar_menu;
    private TextView username_menu;
    private TextView email_menu;

    //To get the username session
    private String username;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Support toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //To can user a FloatingActionButton and set a click listener
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //If the FloatingActionButton is clicked, open ComplaintAddActivity
                Intent intent = new Intent(getBaseContext(), ComplaintAddActivity.class);
                startActivity(intent);
            }
        });


        //To initialize the drawer layout and other views
        initDrawerLayout();
        setDefaultItem();
        initViews();



        getUserData();
        validateSession();
    }

    private void initViews(){
        avatar_menu = (ImageView) findViewById(R.id.avatar_menu);
        username_menu = (TextView) findViewById(R.id.username_menu);
        email_menu = (TextView) findViewById(R.id.email_menu);
    }

    //Init the Drawer Layout
    private void initDrawerLayout() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    //To set a default item selected of the navigation drawer
    private void setDefaultItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.nav_categories) {
            fragmentClass = CategoriesFragment.class;
            toolbar.setTitle("Categorias");
            fab.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_home) {
            fragmentClass = ComplaintsFragment.class;
            toolbar.setTitle("Denuncias");
            fab.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_my_complaints) {
            fragmentClass = MyComplaintsFragment.class;
            toolbar.setTitle("Mis Denuncias");
            fab.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("userId", Remember.getInt("userId", 0));
            intent.putExtra("username", username);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            logout();
        }
        /**
         * Here we replace the fragments. These will be hosted in a FrameLayout
         * that is inside the content_main (layout).
         */
        try {
            //can be null
            assert fragmentClass != null;
            fragment = (Fragment) fragmentClass.newInstance();

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //To close the navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //This method validates that the session is active
    private void validateSession() {
        if (Remember.getString("access_token", "").isEmpty()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    //This method delete the access token and close the session
    private void logout() {
        Remember.putString("access_token", "");
        validateSession();
    }

    //To get the user data
    private void getUserData() {
        Call<User> call = Api.instance().getProfile(Remember.getString("access_token", ""), Remember.getInt("userId", 0));
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.body().getUsername() == null) {
                    username = response.body().getEmail();
                } else {
                    username = response.body().getUsername();
                }

                email = response.body().getEmail();

                setDataMenu();
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Toast.makeText(MainActivity.this, "Comprueba tu conexión a internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDataMenu(){
        //To set dinamically data in the drawer menu
        View header = navigationView.getHeaderView(0);
        avatar_menu = header.findViewById(R.id.avatar_menu);
        avatar_menu.setImageResource(R.drawable.user);

        username_menu = header.findViewById(R.id.username_menu);
        username_menu.setText(username);

        email_menu = header.findViewById(R.id.email_menu);
        email_menu.setText(email);

        System.out.println("email: " + email);

        System.out.println("name: " + username);
    }

}
