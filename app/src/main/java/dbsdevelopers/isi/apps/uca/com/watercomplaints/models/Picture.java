package dbsdevelopers.isi.apps.uca.com.watercomplaints.models;

import java.util.ArrayList;

/**
 * Created by macyarin on 3/11/17.
 */


public class Picture {
    private String title;
    private String url;
    private int complaintId;
    private boolean enabled;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(int complaintId) {
        this.complaintId = complaintId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


}
