package dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters.CategoriesAdapter;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Category;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<Category> category;
    private ProgressBar progressBar;
    private LinearLayout progressBarContainer;
    private SwipeRefreshLayout swipeRefreshLayout;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        //Init ProgressBar
        progressBar = (ProgressBar) view.findViewById(R.id.complaint_progress_loading);
        progressBarContainer = (LinearLayout) view.findViewById(R.id.complaint_progress);

        initViews(view);
        init(view);
        initActions(view);
        setStylesViews();
        return view;
    }


    //Init Views
    private void initViews(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.complaint_progress_loading);
        progressBarContainer = (LinearLayout) view.findViewById(R.id.complaint_progress);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
    }


    //Init actions events
    private void initActions(View view) {
        final View _view = view;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                init(_view);
            }
        });
    }

    //This method sets styles to views
    private void setStylesViews() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }


    /**
     * To get data from the API
     *
     * @param view
     */
    private void init(View view) {
        recyclerView = view.findViewById(R.id.recycler_view_categories);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        /**
         * Make a http request
         */
        Call<List<Category>> call = Api.instance().getCategories(Remember.getString("access_token", ""));
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(@NonNull Call<List<Category>> call, @NonNull Response<List<Category>> response) {
                if (response.body() != null) {
                    progressBarContainer.setVisibility(progressBarContainer.INVISIBLE);
                    recyclerView.setVisibility(recyclerView.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);

                    //Instance a adapter
                    CategoriesAdapter adapter = new CategoriesAdapter(response.body());

                    //Set adapter to the recyclerView
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Category>> call, @NonNull Throwable t) {
                Toast.makeText(getContext(), R.string.message_connection_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
