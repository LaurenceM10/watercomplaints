package dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.ComplaintDetailActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.ProfileActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Picture;

/**
 * Created by macyarin on 11/10/17.
 */


public class ComplaintsAdapter extends RecyclerView.Adapter<ComplaintsAdapter.ViewHolder> {
    private List<Complaint> complaints;
    private Context context;
    public boolean status;

    public ComplaintsAdapter() {
    }

    //Constructor of the adapter
    public ComplaintsAdapter(Context context, List<Complaint> complaints) {
        this.complaints = complaints;
        this.context = context;
        status = false;

        //To sort the list of complaints
        sortComplaints();
    }

    //To sort the list of complaints and show the complaints in an orderly manner
    private void sortComplaints() {
        Collections.reverse(complaints);
    }

    //View holder pattern
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView username;
        public SimpleDraweeView avatar;
        public TextView title;
        public CardView card;
        public CarouselView carousel;
        public TextView date;


        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title_complaint);
            avatar = (SimpleDraweeView) view.findViewById(R.id.avatar);
            username = (TextView) view.findViewById(R.id.username);
            carousel = (CarouselView) view.findViewById(R.id.carousel);
            card = (CardView) view.findViewById(R.id.card);
            date = (TextView) view.findViewById(R.id.date);
        }
    }

    //To inflate the layout
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.complaint_card_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    //Set data in elements of the layout
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //Init objects with data
        final Complaint complaint = complaints.get(position);
        final List<Picture> pictures = complaint.getPictures();

        //To set a beautiful format date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        PrettyTime prettyTime = new PrettyTime();

        //For each iteration make status false
        status = false;

        //There could be an error
        try {
            simpleDateFormat.parse(complaint.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Set the Pretty Time Format in the card
        holder.date.setText(prettyTime.format(simpleDateFormat.getCalendar()));

        //Set a static avatar because there isn't a Avatar Resource in the API
        holder.avatar.setImageResource(R.drawable.user);

        //If the user does not have a defined username their email account is used
        if (complaint.getUser().getUsername() == null) {
            holder.username.setText(complaint.getUser().getEmail());
        } else {
            holder.username.setText(complaint.getUser().getUsername());
        }

        //Set title of the Complaint
        holder.title.setText(complaint.getTitle());

        //Set the number of pages of the Carousel
        holder.carousel.setPageCount(complaint.getPictures().size());

        //If there are no pictures a static background color is set instead of the carousel
        if (complaint.getPictures().size() == 0) {
            //Do nothing
        } else if (complaint.getPictures().size() == 1) {
            //If it is only a picture, the indicator slider is invisible
            holder.carousel.setIndicatorVisibility(View.INVISIBLE);
        } else {
            holder.carousel.setIndicatorVisibility(View.VISIBLE);
        }

        //Insert the url of the images in the ImageView of the customCarouselView
        holder.carousel.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int pos, ImageView imageView) {
                Picasso.with(context).load(pictures.get(pos).getUrl()).into(imageView);
            }
        });


        //If a card is clicked, an activity with full information is opened
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status = true;

                Intent intent = new Intent(view.getContext(), ComplaintDetailActivity.class);
                //The id of the complaint is passed as a reference
                intent.putExtra("id", complaint.getId());
                context.startActivity(intent);
            }
        });


        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(complaint);
            }
        });
    }

    public boolean status() {
        return status;
    }

    //Show a custom popup dialog with dynamic data
    private void showPopup(final Complaint complaint) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.popup_user_profile);

        //Elements of the dialog
        SimpleDraweeView avatar = (SimpleDraweeView) dialog.findViewById(R.id.avatar);
        TextView username = (TextView) dialog.findViewById(R.id.username);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        Button visitProfile = (Button) dialog.findViewById(R.id.visitProfile);

        /**
         * Set dinamyc data in the popup
         */
        avatar.setImageResource(R.drawable.user); //Static because the Avatar resource is not defined in the API

        //If the user does not have a defined username their email account is used
        if (complaint.getUser().getUsername() == null) {
            username.setText(complaint.getUser().getEmail());
        } else {
            username.setText(complaint.getUser().getUsername());
        }

        //Popup properties
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        //To set the animation appear and disappear in the dialog
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().getAttributes().windowAnimations = R.style.AnimationPopup;
        dialog.setCancelable(true);

        //To show popup dialog
        dialog.show();

        //To add a click listener to the close icon
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To close the dialog
                dialog.dismiss();
            }
        });

        visitProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileActivity.class);

                //The id of the complaint is passed as a reference
                intent.putExtra("userId", complaint.getUser().getId());
                if (complaint.getUser().getUsername() == null) {
                    intent.putExtra("username", complaint.getUser().getEmail());
                } else {
                    intent.putExtra("username", complaint.getUser().getUsername());
                }

                context.startActivity(intent);
            }
        });
    }


    //Count the elements of the list
    @Override
    public int getItemCount() {
        return complaints.size();
    }

}
