package dbsdevelopers.isi.apps.uca.com.watercomplaints.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.tumblr.remember.Remember;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.helpers.RandomString;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Category;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.ComplaintCreate;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.ComplaintModel;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Location;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Picture;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintAddActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private EditText title;
    private EditText description;
    private ImageView pictures;
    private ImageButton places;
    private Button create;
    private ProgressDialog progressDialog;
    private boolean error;
    private GoogleApiClient mGoogleApiClient;
    private Location location;
    private Spinner categories;

    //To work with Firebase Storage
    private StorageReference mStorageRef;
    private ArrayList<Uri> globalUri = null;
    private ArrayList<String> generatedUrls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_add);
        initViews();
        initObjects();
        requestPermissions();
        showProgressDialog();
        getCategories();

        //The format correct make error
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //error
        error = false;
        initActions();
    }


    //To init some objects
    private void initObjects() {
        //To can use a Google API Client
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        //To can use a object Location
        location = new Location();

        //To can use Firebase Storage
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }


    /**
     * Method responsible for requesting permission to use photos that
     * are found on the device.
     */
    private void requestPermissions() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    /**
     * Method that initializes the elements of the views (of the layout).
     * These references are obtained through the id that the elements of the view itself possess.
     */
    private void initViews() {
        title = (EditText) findViewById(R.id.title);
        description = (EditText) findViewById(R.id.description);
        create = (Button) findViewById(R.id.create);
        pictures = (ImageView) findViewById(R.id.pictures);
        categories = (Spinner) findViewById(R.id.categories);
        places = (ImageButton) findViewById(R.id.places);
    }


    /**
     * This method initiates actions from events on some
     * elements of the view.
     */
    private void initActions() {
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.required_title, Toast.LENGTH_LONG).show();
                } else if (description.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.required_description, Toast.LENGTH_LONG).show();
                } else if (location == null) {
                    Toast.makeText(ComplaintAddActivity.this, R.string.required_location, Toast.LENGTH_SHORT).show();
                } else if (categories.getSelectedItemPosition() == 0) {
                    Toast.makeText(ComplaintAddActivity.this, R.string.required_category, Toast.LENGTH_SHORT).show();
                } else if (location.getLng() == 0.0 && location.getLat() == 0.0) {
                    Toast.makeText(ComplaintAddActivity.this, R.string.required_location, Toast.LENGTH_SHORT).show();
                } else if (globalUri != null) {
                    //To create a Dialog with circular progress
                    progressDialog.show();
                    upload(globalUri);
                } else {
                    Toast.makeText(ComplaintAddActivity.this, R.string.required_pictures, Toast.LENGTH_SHORT).show();
                }
            }
        });

        pictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<com.nguyenhoanglam.imagepicker.model.Image> images = new ArrayList<>();

                //To start the Image Picker
                ImagePicker.with(ComplaintAddActivity.this) //  Initialize ImagePicker with activity or fragment context
                        .setToolbarColor("#2196F3")         //  Toolbar color
                        .setStatusBarColor("#1976D2")       //  StatusBar color (works with SDK >= 21  )
                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                        .setProgressBarColor("#2196F3")     //  ProgressBar color
                        .setBackgroundColor("#FFFFFF")      //  Background color
                        .setCameraOnly(false)               //  Camera mode
                        .setMultipleMode(true)              //  Select multiple images or single image
                        .setFolderMode(true)                //  Folder mode
                        .setShowCamera(true)                //  Show camera button
                        .setFolderTitle("Albumes")           //  Folder title (works with FolderMode = true)
                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                        .setDoneTitle("Hecho")               //  Done button title
                        .setLimitMessage("Has alcanzado el límite de selección")    // Selection limit message
                        .setMaxSize(5)                      //  Max images can be selected
                        .setSavePath("ImagePicker")         //  Selected images
                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
                        .setSelectedImages(images)
                        .start();
            }
        });

        //To open Google Places Picker when the user click the button
        places.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    displayPlacePicker();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    //To open places picker
    private void displayPlacePicker() throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        int PLACE_PICKER_REQUEST = 1;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        startActivityForResult(builder.build(ComplaintAddActivity.this), PLACE_PICKER_REQUEST);
    }


    /**
     * Method responsible for uploading one or more images to Firebase Storage.
     * This method receives as a parameter the path of the images in local (on the device).
     */
    private void upload(final ArrayList<Uri> file) {
        StorageReference riversRef = null;
        generatedUrls = new ArrayList<>();

        //To advance a position
        for (Uri aFile : file) {
            //set error to false
            error = false;

            //To generate a random string and build a route
            riversRef = mStorageRef.child("images/" + RandomString.generateString() + ".jpg");

            //Upload a file in Firebase for each iteration
            riversRef.putFile(aFile)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // Get a URL to the uploaded content
                            generatedUrls.add(taskSnapshot.getDownloadUrl().toString());

                            //Validate the size of the uri and the publics urls  of firebase
                            if (generatedUrls.size() == file.size()) {
                                create(generatedUrls);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //To create a Dialog with circular progress
                            error = true;
                            progressDialog.dismiss();
                            cleanObjects(); //To clear the elements of the objects and can't use the same data
                            customDialog(); //To show information
                        }
                    });

            if (error) {
                break;
            }
        }
    }


    /**
     * To create a complaint. The first step is to validate that all data is correct.
     */
    private void create(final ArrayList<String> url) {

        //To get and store a date of the API
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Calendar cal = Calendar.getInstance();


        //Object with data to create a Complaint
        ComplaintCreate complaint = new ComplaintCreate();
        complaint.setTitle(title.getText().toString());
        complaint.setDescription(description.getText().toString());
        complaint.setCategoryId(categories.getSelectedItemPosition());
        complaint.setLocation(location);
        complaint.setCreatedAt(simpleDateFormat.format(cal.getTime()));
        complaint.setEnabled(true);


        /**
         * The complaint is created by accessing the createComplaint method found in the API class,
         * This method receives 2 parameters that were sent at the end through the HTTP POST request.
         * The first is the access token, and the second is the Complaint object that contains all the data of
         * the complaint.
         */
        Call<ComplaintModel> call = Api.instance().createComplaint(Remember.getString("access_token", ""), complaint);
        call.enqueue(new Callback<ComplaintModel>() {
            @Override
            public void onResponse(@NonNull Call<ComplaintModel> call, @NonNull Response<ComplaintModel> response) {
                if (response.body() != null) {
                    //Call the method that publishes images
                    publishPictures(url, response.body().getId());
                } else {
                    //If there's a null response
                    hideProgressDialog();

                    cleanObjects(); //To clear the elements of the objects and can't use the same data
                    customDialog(); //To show information
                }
            }

            @Override
            public void onFailure(@NonNull Call<ComplaintModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                cleanObjects(); //To clear the elements of the objects and can't use the same data
                customDialog(); //To show information
            }
        });

        cleanInput();
    }


    /**
     * This method obtains the result generated by AlbumSelectActivity
     * (the result that interests us is the Uri of the or the images that the user selects).
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<com.nguyenhoanglam.imagepicker.model.Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            ArrayList<Uri> uri = new ArrayList<>();
            for (int i = 0; i < images.size(); i++) {
                uri.add(Uri.fromFile(new File(images.get(i).getPath())));
                addImageView(uri.get(0));
                globalUri = uri;
            }
        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

                //Get the location and set in the object of type location
                try {
                    location.setLat(place.getLatLng().latitude);
                    location.setLng(place.getLatLng().longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * To clean the edit texts after creating a complaint
     */
    public void cleanInput() {
        title.setText("");
        description.setText("");
        pictures.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
    }

    //To clean the imageView
    public void cleanViews() {
        addImageView(null);
    }

    //To delete the elements of the objects
    public void cleanObjects() {
        globalUri = null;
    }


    /**
     * To inflate a options menu in the activity
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_complaint, menu);
        return true;
    }


    /**
     * To temporarily set an image in the view
     */
    private void addImageView(Uri file) {
        pictures.setImageURI(file);
    }

    /**
     * To upload images to the API and relate them to a complaint
     */
    private void publishPictures(ArrayList<String> url, final int id) {
        //set error to false
        error = false;

        //Object from which to create and save pictures in the API
        final Picture picture = new Picture();

        //To  iterate the ArrayList of urls
        for (String anUrl : url) {

            //To establish the data of a picture in each iteration
            picture.setTitle("Imagen de la denuncia");
            picture.setComplaintId(id);
            picture.setUrl(anUrl);
            picture.setEnabled(true);

            //This make a http request to create or publish pictures in the API
            Call<Picture> call = Api.instance().createPictures(Remember.getString("access_token", ""), picture);
            call.enqueue(new Callback<Picture>() {

                @Override
                public void onResponse(@NonNull Call<Picture> call, @NonNull Response<Picture> response) {
                    if (response.body() != null) {
                        Log.i("Debug:", "Se subió");
                        Toast.makeText(ComplaintAddActivity.this, R.string.create_complaint_success, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Picture> call, @NonNull Throwable t) {
                    error = true;
                    //If there is an error when publishing a photo, the last Complaint is deleted
                    Call<ResponseBody> callDelete = Api.instance().deleteComplaint(Remember.getString("access_token", ""), id);
                    callDelete.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                            Toast.makeText(ComplaintAddActivity.this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                            Toast.makeText(ComplaintAddActivity.this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
                        }
                    });


                    //To hide and show a message error when publishing a picture
                    hideProgressDialog();
                    cleanObjects(); //To clear the elements of the objects and can't use the same data
                    customDialog();
                    customDialog();
                }

            });

        }

        //If there's not an error, close the Activity
        if (!error) {
            this.finish();
        }


        /**
         * To hide and show an error message when publishing an image
         * regardless of whether there is an error or not
         */
        hideProgressDialog();
        cleanObjects(); //To clear the elements of the objects and can't use the same data
    }


    //When there is no connection, an error message of red is displayed
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
    }

    /**
     * This method get categories from the API
     */
    private void getCategories() {
        Call<List<Category>> call = Api.instance().getCategories(Remember.getString("access_token", ""));
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(@NonNull Call<List<Category>> call, @NonNull Response<List<Category>> response) {
                List<String> listCategories = new ArrayList<String>();
                for (Category category : response.body()) {
                    listCategories.add(category.getName());
                }

                //To create and set data in the Spinner
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ComplaintAddActivity.this,
                        android.R.layout.simple_spinner_item, listCategories);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categories.setAdapter(dataAdapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Category>> call, @NonNull Throwable t) {
                customDialog();
            }
        });
    }

    //To create and show a custom dialog to show information
    private void customDialog() {
        final Dialog dialog = new Dialog(this);

        //To set a layout inside the dialog
        dialog.setContentView(R.layout.dialog_information);

        /**
         * To set an animation when the dialog appear and disappear.
         * The warning is because getAttributes() might not find the animation and return null
         */
        dialog.getWindow().getAttributes().windowAnimations = R.style.AnimationPopup;

        //Objects analogous to the layout elements of the dialog
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        Button accept = (Button) dialog.findViewById(R.id.accept);

        //Set content to Dialog with string resources
        title.setText(getString(R.string.title_complaint_error));
        message.setText(getString(R.string.message_complaint_error));

        //When the user click accept button the dialog is closed
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    //To create a simple ProgressDialog with a message
    private void showProgressDialog() {
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Creando la denuncia");
        progressDialog.setCancelable(false);
    }

    //To hide a simple ProgressDialog
    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

}



