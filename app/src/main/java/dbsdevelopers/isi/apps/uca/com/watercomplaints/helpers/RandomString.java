package dbsdevelopers.isi.apps.uca.com.watercomplaints.helpers;

/**
 * Created by macyarin on 4/11/17.
 */

import java.util.UUID;

public class RandomString {

    public static void main(String[] args) {
        System.out.println(generateString());
    }

    /**
     * Return a random string
     *
     * @return
     */
    public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }
}
