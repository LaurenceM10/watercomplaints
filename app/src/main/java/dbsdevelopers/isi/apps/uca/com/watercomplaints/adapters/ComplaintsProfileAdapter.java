package dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.activities.ComplaintDetailActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Picture;

/**
 * Created by macyarin on 16/11/17.
 */

public class ComplaintsProfileAdapter extends RecyclerView.Adapter<ComplaintsProfileAdapter.ViewHolder> {
    public Activity context;
    private List<Complaint> complaints;
    private List<Complaint> myComplaints;
    private View recyclerView;
    private View content;
    private int userId;


    //Elements of the Popup
    private SimpleDraweeView avatar;


    public ComplaintsProfileAdapter(Activity context, List<Complaint> complaints, View recyclerView, View content, int userId) {
        this.context = context;
        this.complaints = complaints;
        this.recyclerView = recyclerView;
        this.content = content;
        this.userId = userId;

        sortComplaints();
        getMyComplaints();
    }

    //To get just the complaints of the active user
    private void getMyComplaints() {
        myComplaints = new ArrayList<>();

        //Iterate complaints list
        for (Complaint complaint : complaints) {
            //Add all matches to myComplaints when a specific user ID matches
            if (complaint.getUser().getId() == userId) {
                myComplaints.add(complaint);
            }
        }
    }

    //To sort the list of complaints and show the complaints in an orderly manner
    private void sortComplaints() {
        Collections.reverse(complaints);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (SimpleDraweeView) itemView.findViewById(R.id.image);
        }
    }

    @Override
    public ComplaintsProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_image, parent, false);

        ComplaintsProfileAdapter.ViewHolder viewHolder = new ComplaintsProfileAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ComplaintsProfileAdapter.ViewHolder holder, int position) {
        final Complaint complaint = myComplaints.get(position);
        final List<Picture> pictures = complaint.getPictures();

        try {
            holder.image.setImageURI(pictures.get(0).getUrl());
        } catch (Exception e) {
            holder.image.setImageURI("http://1.bp.blogspot.com/-fIIXrDNarGE/UGo3e-o_f9I/AAAAAAAAGEs/f7SmLFsIhjg/w1200-h630-p-k-no-nu/landscape_hd.jpg");
        }


        //If the user clicks on the image
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ComplaintDetailActivity.class);
                //The id of the complaint is passed as a reference
                intent.putExtra("id", complaint.getId());
                context.startActivity(intent);
            }
        });

        //If the user presses the image for several seconds
        holder.image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showDialog(complaint, pictures);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return myComplaints.size();
    }

    private void showDialog(Complaint complaint, final List<Picture> pictures) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.popup_card_view);
        CarouselView image = (CarouselView) dialog.findViewById(R.id.carousel);
        TextView title = (TextView) dialog.findViewById(R.id.title_complaint);
        TextView username = (TextView) dialog.findViewById(R.id.username);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);


        //Set dinamically content in the popup dialog
        try {
            //Set the username in the popup
            //If the user does not have a defined username their email account is used
            if (complaint.getUser().getUsername() == null) {
                username.setText(complaint.getUser().getEmail());
            } else {
                username.setText(complaint.getUser().getUsername());
            }


            //Insert the url of the images in the ImageView of the customCarouselView
            image.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int pos, ImageView imageView) {
                    Picasso.with(context).load(pictures.get(pos).getUrl()).into(imageView);
                }
            });

            //Set the number of pages in the Carousel
            image.setPageCount(complaint.getPictures().size());

            //If there are no pictures a static background color is set instead of the carousel
            if (complaint.getPictures().size() == 0) {
                //Do nothing
            } else if (complaint.getPictures().size() == 1) {
                //If it is only a picture, the indicator slider is invisible
                image.setIndicatorVisibility(View.INVISIBLE);
            } else {
                image.setIndicatorVisibility(View.VISIBLE);
            }

            //Set a title complaint
            title.setText(complaint.getTitle());
        } catch (Exception e) {
            //Set the username in the popup
            username.setText(complaint.getUser().getUsername());

            //Set a default image
            image.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
                    Picasso.with(context).load("http://1.bp.blogspot.com/-fIIXrDNarGE/UGo3e-o_f9I/AAAAAAAAGEs/f7SmLFsIhjg/w1200-h630-p-k-no-nu/landscape_hd.jpg");
                }
            });

            //Set a title complaint
            title.setText(complaint.getTitle());
        }

        //Popup properties
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        //To set the animation appear and disappear in the dialog
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().getAttributes().windowAnimations = R.style.AnimationPopup;
        dialog.setCancelable(true);

        //To show popup dialog
        dialog.show();


        /**
         * To close de dialog
         */
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

}












