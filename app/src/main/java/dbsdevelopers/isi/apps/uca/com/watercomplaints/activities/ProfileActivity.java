package dbsdevelopers.isi.apps.uca.com.watercomplaints.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters.ComplaintsProfileAdapter;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private SimpleDraweeView avatar;
    private TextView complaintsNumber;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private CollapsingToolbarLayout toolbarLayout;
    private LinearLayout content;

    private ProgressBar progressBar;
    private LinearLayout progressBarContainer;


    //To get and save data from Intent
    private int userId;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //To get extras
        Intent intent = getIntent();
        getDataIntent(intent);

        //set icon in toolbar
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        initViews();
        setProfileData();
        getComplaints();
    }

    /**
     * To init views getting the reference of the layout elements
     */
    private void initViews() {
        avatar = (SimpleDraweeView) findViewById(R.id.avatar);
        complaintsNumber = (TextView) findViewById(R.id.complaintsNumber);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        content = (LinearLayout) findViewById(R.id.content);
        progressBar = (ProgressBar) findViewById(R.id.complaint_progress_loading);
        progressBarContainer = (LinearLayout) findViewById(R.id.complaint_progress);
    }


    //To get data from the intent
    private void getDataIntent(Intent intent) {
        userId = intent.getExtras().getInt("userId");
        username = intent.getExtras().getString("username");
    }

    //This method obtains user data and establishes it in the profile header
    private void setProfileData() {
        toolbarLayout.setTitle(username);
        avatar.setImageResource(R.drawable.user);
    }

    //This method get the complaints of the user
    private void getComplaints() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3)); //Grid of 3 columns
        recyclerView.setNestedScrollingEnabled(false);

        Call<List<Complaint>> call = Api.instance().getComplaints(Remember.getString("access_token", ""));
        call.enqueue(new Callback<List<Complaint>>() {
            @Override
            public void onResponse(@NonNull Call<List<Complaint>> call, @NonNull Response<List<Complaint>> response) {
                if (response.body() != null) {
                    //Disappear the progressBar when the data is ready
                    progressBarContainer.setVisibility(View.INVISIBLE);

                    //To count the complaints of the user
                    countComplaints(response.body());

                    //To set an adapter to the RecyclerView
                    recyclerView.setAdapter(new ComplaintsProfileAdapter(ProfileActivity.this, response.body(), recyclerView, content, userId));
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    Log.i("DEBUG: ", "Error" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Complaint>> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), "Verifica tu conexión a internet", Toast.LENGTH_SHORT).show();
                Log.i("Denuncias", "Error");
            }
        });
    }


    /**
     * This method count the complaints.
     */
    private void countComplaints(List<Complaint> complaints) {
        ArrayList<Complaint> myComplaint = new ArrayList<>();
        for (Complaint complaint : complaints) {
            if (complaint.getUser().getId() == userId) {
                myComplaint.add(complaint);
            }
        }
        complaintsNumber.setText(myComplaint.size() + " Denuncias");
    }


    //To get the item selected and init any action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //To create the menu with options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
