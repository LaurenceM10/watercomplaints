package dbsdevelopers.isi.apps.uca.com.watercomplaints.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.tumblr.remember.Remember;

import java.io.File;
import java.util.ArrayList;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.MainActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.helpers.RandomString;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.AccessToken;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.User;
import de.hdodenhof.circleimageview.CircleImageView;
import in.myinnos.awesomeimagepicker.activities.AlbumSelectActivity;
import in.myinnos.awesomeimagepicker.helpers.ConstantsCustomGallery;
import in.myinnos.awesomeimagepicker.models.Image;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    private EditText username;
    private EditText email;
    private EditText password;
    private ProgressBar progressBar;
    private Button signUp;
    private Button signIn;
    private StorageReference mStorageRef;
    private Uri firebaseUrl;
    private CircleImageView avatar;
    private Uri globalUri;

    //ProgressBar Dialog
    private ProgressDialog progressDialog;

    //Elements of the Dialog Custom error
    private TextView title;
    private TextView message;
    private Button accept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Default value
        globalUri = null;

        //init firebase
        mStorageRef = FirebaseStorage.getInstance().getReference();

        initViews();
        setActions();
    }


    /**
     * To init views
     */
    private void initViews() {
        username = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        signUp = (Button) findViewById(R.id.signUp);
        signIn = (Button) findViewById(R.id.signIn);
        avatar = (CircleImageView) findViewById(R.id.avatar);
    }


    /**
     * Init logic actions
     */
    private void setActions() {
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalUri != null) {
                    uploadAvatar(globalUri);
                }
                signUp();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }


    /**
     * To signup a user
     */
    private void signUp() {
        if (username.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.required_username, Toast.LENGTH_LONG).show();
        } else if (email.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.required_email, Toast.LENGTH_LONG).show();
        } else if (password.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.required_password, Toast.LENGTH_LONG).show();
        } else {
            //Set a user in JsonObject to create a user in the API
            JsonObject object = new JsonObject();
            object.addProperty("username", username.getText().toString());
            object.addProperty("email", email.getText().toString());
            object.addProperty("password", password.getText().toString());

            //ProgressBar dialog
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            progressDialog.setMessage(getString(R.string.progress_bar_loading));
            progressDialog.setCancelable(false);
            progressDialog.show();


            //Dialog information
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_information);
            title = (TextView) dialog.findViewById(R.id.title);
            message = (TextView) dialog.findViewById(R.id.message);
            accept = (Button) dialog.findViewById(R.id.accept);

            //Set content to Dialog
            title.setText(getString(R.string.title_invalid_data));
            message.setText(getString(R.string.message_invalid_data));
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            // create call
            Call<JsonObject> call = Api.instance().signUp(object);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        loginRequest(email.getText().toString(), password.getText().toString());
                    } else {
                        //If the data is wrong
                        progressDialog.dismiss();
                        dialog.show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    //If there is no connection with the server
                    progressDialog.dismiss();
                    Toast.makeText(SignUpActivity.this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    /**
     * This method make a http request to init session of the user.
     * The response is an access token, which we keep to use while the session is active
     *
     * @param email
     * @param password
     */
    private void loginRequest(String email, String password) {
        // instance a user
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        // create call
        Call<AccessToken> call = Api.instance().login(user);
        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                if (response.body() != null) {
                    Log.i("access_token", response.body().getId());
                    Remember.putInt("userId", response.body().getUserId());
                    Remember.putString("access_token", response.body().getId(), new Remember.Callback() {
                        @Override
                        public void apply(Boolean success) {
                            if (success) {
                                Toast.makeText(getApplicationContext(), "Te has registrado exitosamente", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {

            }
        });
    }


    /**
     * To upload a picture of the user's  avatar to Firebase Storage.
     * This method receives as parameter a URI with the direction local file
     *
     * @param avatarUri
     */
    private void uploadAvatar(Uri avatarUri) {
        StorageReference riversRef = mStorageRef.child("images/" + RandomString.generateString() + ".jpg");

        riversRef.putFile(avatarUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Obtiene la url de la imagen recién subida
                        firebaseUrl = taskSnapshot.getDownloadUrl();
                        Log.i("URL Firebase: ", "" + firebaseUrl);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                });
    }


    /**
     * Init AlbumSelectActivity to select an image
     */
    private void selectImage() {
        Intent intent = new Intent(getApplicationContext(), AlbumSelectActivity.class);
        intent.putExtra(ConstantsCustomGallery.INTENT_EXTRA_LIMIT, 1);
        startActivityForResult(intent, ConstantsCustomGallery.REQUEST_CODE);
    }


    /**
     * To temporarily set an image in the view
     *
     * @param image
     */
    private void addAvatarView(Uri image) {
        avatar.setImageURI(image);
        avatar.setPadding(0, 0, 0, 0);
    }


    /**
     * To request permits
     */
    private void requestPermissions() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }


    /**
     * This method obtains the result generated by AlbumSelectActivity
     * (the result that interests us is the Uri of the image that the user selects).
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ConstantsCustomGallery.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            ArrayList<Image> images = data.getParcelableArrayListExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES);

            for (int i = 0; i < images.size(); i++) {
                globalUri = Uri.fromFile(new File(images.get(i).path));
                addAvatarView(globalUri);
            }
        }
    }

}
