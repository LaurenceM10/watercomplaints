package dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters.MyComplaintsAdapter;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyComplaintsFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<Complaint> ownerComplaints;
    private ProgressBar progressBar;
    private LinearLayout progressBarContainer;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;


    public MyComplaintsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_complaints, container, false);

        //Call all the methods
        initViews(view);
        init(view);
        initActions(view);
        setStylesViews();
        return view;
    }


    //Init Views
    private void initViews(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.complaint_progress_loading);
        progressBarContainer = (LinearLayout) view.findViewById(R.id.complaint_progress);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
    }


    //Init actions events
    private void initActions(View view) {
        final View _view = view;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                init(_view);
            }
        });
    }

    //This method sets styles to views
    private void setStylesViews() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }


    private void init(View view) {
        //get reference of the recyclerView
        recyclerView = view.findViewById(R.id.recycler_view_owner_complaints);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create call to make a http request
        Call<List<Complaint>> call = Api.instance().getComplaints(Remember.getString("access_token", ""));
        call.enqueue(new Callback<List<Complaint>>() {

            @Override
            public void onResponse(@NonNull Call<List<Complaint>> call, @NonNull Response<List<Complaint>> response) {
                if (response.body() != null) {
                    //Stop progress when the information is ready
                    progressBarContainer.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);

                    //Stop swipe refresh when the information is ready
                    swipeRefreshLayout.setRefreshing(false);

                    //Pass data to the adapter
                    MyComplaintsAdapter complaints = new MyComplaintsAdapter(getContext(), response.body());
                    //Set adapter to RecyclerView
                    recyclerView.setAdapter(complaints);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Complaint>> call, @NonNull Throwable t) {
                //Stop progress when information fails
                progressBarContainer.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                // If the data fails a message is displayed to the user
                Toast.makeText(getContext(), "Verifica tu conexión a internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        init(view);
    }

}
