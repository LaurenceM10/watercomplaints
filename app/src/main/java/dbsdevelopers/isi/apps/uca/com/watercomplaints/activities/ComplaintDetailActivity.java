package dbsdevelopers.isi.apps.uca.com.watercomplaints.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.tumblr.remember.Remember;

import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Location;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Picture;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private CarouselView image;
    private SimpleDraweeView avatar;
    private TextView username;
    private TextView title;
    private TextView description;
    private int id; //Id to get Extras in the intent
    private Location location;
    private Spinner categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_detail);

        //Get extras
        Intent intent = getIntent();

        initViews();
        initObjects();
        getDataIntent(intent);
        getData();

        //Arrow back support
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (location != null) {
            initGoogleMapsFragment();
        }
        initGoogleMapsFragment();
    }

    /**
     * To init view
     */

    private void initViews() {
        image = (CarouselView) findViewById(R.id.image);
        avatar = (SimpleDraweeView) findViewById(R.id.avatar);
        username = (TextView) findViewById(R.id.username);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        categories = (Spinner) findViewById(R.id.categories);
    }

    //To init the necesarys objects
    private void initObjects() {
        location = new Location();
    }


    /**
     * To init maps
     */
    private void initGoogleMapsFragment() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * to get the id of the intent extras
     */
    private void getDataIntent(Intent intent) {
        id = intent.getExtras().getInt("id", 0);
    }

    /**
     * Get data from the API.
     * This method make a http request to get data
     */
    private void getData() {
        Call<Complaint> call = Api.instance().getComplaint(Remember.getString("access_token", ""), id);
        call.enqueue(new Callback<Complaint>() {
            @Override
            public void onResponse(@NonNull Call<Complaint> call, @NonNull Response<Complaint> response) {
                if (response.body() != null) {
                    final Complaint complaint = response.body();
                    assert complaint != null;
                    final List<Picture> pictures = complaint.getPictures();

                    //Set data from API in layout elements
                    avatar.setImageResource(R.drawable.user);

                    //If the user does not have a defined username their email account is used
                    if (complaint.getUser().getUsername() == null) {
                        username.setText(complaint.getUser().getEmail());
                    } else {
                        username.setText(complaint.getUser().getUsername());
                    }

                    title.setText(complaint.getTitle());
                    description.setText(complaint.getDescription());
                    location.setLat(complaint.getLocation().getLat());
                    location.setLng(complaint.getLocation().getLng());

                    //When the latitude and longitude is ready, init the map and set the marker
                    initGoogleMapsFragment();


                        image.setImageListener(new ImageListener() {
                            @Override
                            public void setImageForPosition(int pos, ImageView imageView) {
                                if (complaint.getPictures().size() == 0) {
                                    Log.i("Debug:", "Here is the image debug");
                                    Picasso.with(ComplaintDetailActivity.this).load("http://www.pixedelic.com/themes/geode/demo/wp-content/uploads/sites/4/2014/04/placeholder2.png").into(imageView);
                                } else {
                                    Log.i("Debug:", "There is a image");
                                    Picasso.with(ComplaintDetailActivity.this).load(pictures.get(pos).getUrl()).into(imageView);
                                }
                            }
                        });
                    //Insert the url of the images in the ImageView of the customCarouselView


                    image.setPageCount(pictures.size());

                    //If there are no pictures a static background color is set instead of the carousel
                    if (complaint.getPictures().size() == 0) {
                        //Do nothing
                    } else if (complaint.getPictures().size() == 1) {
                        //If it is only a picture, the indicator slider is invisible
                        image.setIndicatorVisibility(View.INVISIBLE);
                    } else {
                        image.setIndicatorVisibility(View.VISIBLE);
                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<Complaint> call, @NonNull Throwable t) {
                Toast.makeText(ComplaintDetailActivity.this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //When the map is ready, set a marker with the ubication
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLat(), location.getLng()))
                .title("Marker"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLat(), location.getLng())));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Create a Uri from an intent string. Use the result to create an Intent.
                Uri geoLocation = Uri.parse("geo:" + location.getLat() + "," + location.getLng());

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(geoLocation);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }
}


















