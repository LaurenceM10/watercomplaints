package dbsdevelopers.isi.apps.uca.com.watercomplaints.api;

import com.google.gson.JsonObject;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.AccessToken;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Category;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.ComplaintCreate;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.ComplaintModel;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Picture;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ApiInterface {

    /**
     * Get all the categories
     */
    @GET("Categories")
    Call<List<Category>> getCategories(@Header("Authorization") String authorization);


    /**
     * Get all the complaints
     */
    @GET("Complaints?filter={\"include\": [\"pictures\", \"user\"]}")
    Call<List<Complaint>> getComplaints(@Header("Authorization") String authorization);


    /**
     * Get complaints, user and pictures relations for complaint id
     */
    @GET("Complaints/{id}?filter={\"include\": [\"pictures\", \"user\"]}")
    Call<Complaint> getComplaint(@Header("Authorization") String authorization, @Path("id") int complaintId);


    /**
     * Get complaints from the user session
     */
    @GET("Complaints/{id}?filter={\"include\": [\"pictures\", \"user\"], \"where\": {\"userId\":\" {id} \"}}")
    Call<List<Complaint>> getMyComplaints(@Header("Authorization") String authorization, @Path("id") int id);  // TODO: Hacerlo de forma dinámica con otro parámetro


    /**
     * To create a complaint
     */
    @POST("Complaints")
    Call<ComplaintModel> createComplaint(@Header("Authorization") String authorization, @Body ComplaintCreate complaint);


    /**
     * Create pictures in the API
     */
    @POST("Pictures")
    Call<Picture> createPictures(@Header("Authorization") String authorization, @Body Picture picture);


    /**
     * The request is a user object with the user's login data
     * The response is an Access Token
     **/
    @POST("Users/login")
    Call<AccessToken> login(@Body User user);

    /**
     * To create a user
     */
    @POST("Users")
    Call<JsonObject> signUp(@Body JsonObject object);

    @GET("Users/{id}")
    Call<User> getProfile(@Header("Authorization") String authorization, @Path("id") int userId);


    @DELETE("Complaints/{id}")
    Call<ResponseBody> deleteComplaint(@Header("Authorization") String authorization, @Path("id") int id);

}