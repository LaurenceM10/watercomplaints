package dbsdevelopers.isi.apps.uca.com.watercomplaints.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tumblr.remember.Remember;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.MainActivity;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.AccessToken;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private AnimationDrawable anim;

    //Elements of the login
    private EditText email;
    private EditText password;
    private Button signIn;
    private Button signUp;
    private LinearLayout container;
    private ProgressDialog progressDialog;

    //Elements of the Dialog Custom error
    private TextView title;
    private TextView message;
    private Button accept;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initViews();
        setActions();
        validateSession();
        initAnimations();
    }

    /**
     * Init the animations in the background
     */
    private void initAnimations() {
        anim = (AnimationDrawable) container.getBackground();
        anim.setEnterFadeDuration(6000);
        anim.setExitFadeDuration(9000);
    }

    /**
     * Method that initializes the attributes of the class with the respective reference of the views (of the layout).
     * These references were found through the id that the elements of the view itself have.
     */
    private void initViews() {
        container = (LinearLayout) findViewById(R.id.container);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        signIn = (Button) findViewById(R.id.signIn);
        signUp = (Button) findViewById(R.id.signUp);
    }


    /**
     * Method that initiates actions from events on some elements of the view.
     */
    private void setActions() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * This method validates that the login data is correct, then call the loginRequest function,
     * which is the one in charge of initiating the session.
     */

    private void signIn() {
        if (email.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Escribe tu correo electrónico", Toast.LENGTH_LONG).show();
        } else if (password.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Escribe tu contraseña", Toast.LENGTH_LONG).show();
        } else {
            /**
             * Oculta el teclado
             */
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);


            loginRequest(email.getText().toString(), password.getText().toString());
        }
    }


    /**
     * This method performs the HTTP request to initiate the user's session.
     * Right here we receive an Access Token, which we save to use while the session is active.
     *
     * @param email
     * @param password
     */
    private void loginRequest(String email, String password) {
        // Instanciamos User
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        //To create a Dialog with circular progress
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage(getString(R.string.progress_bar_loading));
        progressDialog.setCancelable(false);
        progressDialog.show();


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_information);
        dialog.getWindow().getAttributes().windowAnimations = R.style.AnimationPopup;
        title = (TextView) dialog.findViewById(R.id.title);
        message = (TextView) dialog.findViewById(R.id.message);
        accept = (Button) dialog.findViewById(R.id.accept);

        //Set content to Dialog
        title.setText(getString(R.string.title_invalid_data));
        message.setText(getString(R.string.message_invalid_data));
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        // Request to init session
        Call<AccessToken> call = Api.instance().login(user);
        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                if (response.body() != null) {
                    Log.i("access_token", response.body().getId());

                    //Save the id of the user
                    Remember.putInt("userId", response.body().getUserId());
                    Log.i("ID del usuario", "" + Remember.getInt("userId", 0));

                    //Save the access token
                    Remember.putString("access_token", response.body().getId(), new Remember.Callback() {
                        @Override
                        public void apply(Boolean success) {
                            if (success) {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } else {
                    progressDialog.dismiss();
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, R.string.message_connection_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        anim.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    private void validateSession() {
        if (!Remember.getString("access_token", "").isEmpty()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
