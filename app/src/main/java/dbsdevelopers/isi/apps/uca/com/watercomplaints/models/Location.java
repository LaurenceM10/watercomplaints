package dbsdevelopers.isi.apps.uca.com.watercomplaints.models;

/**
 * Created by macyarin on 4/11/17.
 */


public class Location {
    private double lat;
    private double lng;


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
