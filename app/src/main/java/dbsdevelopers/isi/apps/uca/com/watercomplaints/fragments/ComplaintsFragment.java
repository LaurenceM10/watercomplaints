package dbsdevelopers.isi.apps.uca.com.watercomplaints.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tumblr.remember.Remember;
import java.util.List;

import dbsdevelopers.isi.apps.uca.com.watercomplaints.R;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.adapters.ComplaintsAdapter;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.api.Api;
import dbsdevelopers.isi.apps.uca.com.watercomplaints.models.Complaint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintsFragment extends Fragment {
    private RecyclerView recyclerView;
    private LinearLayout progressBarContainer;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;

    //To know what activity was recently closed
    private boolean status;
    private ComplaintsAdapter complaintsAdapter;

    public ComplaintsFragment() {
        // Required empty public constructor
        status = false;
        complaintsAdapter = new ComplaintsAdapter();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_complaints, container, false);


        //Call all methods
        initViews(view);
        init(view);
        initActions(view);
        setStylesViews();
        return view;
    }


    //Init Views
    private void initViews(View view) {
        progressBarContainer = (LinearLayout) view.findViewById(R.id.complaint_progress);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
    }


    //Init actions events
    private void initActions(final View view) {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                init(view);
            }
        });
    }

    //This method sets styles to views
    private void setStylesViews() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }


    /**
     * Subsequently, the HTTP request is made to the API which returns the list
     * of complaints in case no error occurs.
     */
    public void init(View view) {
        //get reference of the recyclerView
        recyclerView = view.findViewById(R.id.recycler_view_my_complaints);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //Create call to make a http request
        Call<List<Complaint>> call = Api.instance().getComplaints(Remember.getString("access_token", ""));
        call.enqueue(new Callback<List<Complaint>>() {
            @Override
            public void onResponse(@NonNull Call<List<Complaint>> call, @NonNull Response<List<Complaint>> response) {
                if (response.body() != null) {
                    //Stop progress when the information is ready
                    progressBarContainer.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);

                    //Stop swipe refresh when the information is ready
                    swipeRefreshLayout.setRefreshing(false);

                    //Pass data to the adapter
                    complaintsAdapter = new ComplaintsAdapter(getContext(), response.body());

                    System.out.println(status);
                    //Set adapter to RecyclerView
                    recyclerView.setAdapter(complaintsAdapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Complaint>> call, @NonNull Throwable t) {
                //Stop progress when information fails
                progressBarContainer.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                // If the data fails a message is displayed to the user
                Toast.makeText(getContext(), "Verifica tu conexión a internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

        }
    }


    /**
     * If the ActivityDetail is open and then close the fragment is not updated.
     * But if the ComplaintAddActivity is opened and then the fragment is closed
     * it is updated to show the last complaint
     */
    @Override
    public void onResume() {
        super.onResume();
        if (complaintsAdapter.status()) {
            complaintsAdapter.status = false;
        } else {
            init(view);
        }
    }
}
