package dbsdevelopers.isi.apps.uca.com.watercomplaints;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.tumblr.remember.Remember;

/**
 * Created by macyarin on 11/10/17.
 */

public class ProjectApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Remember.init(getApplicationContext(), "dbsdevelopers.isi.apps.uca.com.watercomplaints");
    }
}
